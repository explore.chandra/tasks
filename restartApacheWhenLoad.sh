# 
check=`cat /proc/loadavg | sed 's/\./ /' | awk '{print $1}'`
# define max load avarage when script is triggered 
max_load='25'
# location to Apache init script
apache_init='/etc/init.d/apache2';

if [ $check -gt "$max_load" ]; then
$apache_init stop
sleep 5;
$apache_init restart
echo "$(date) : Apache Restart due to excessive load | $check |";
else
echo " $(date) : Apache has not excedded the max-load($max_load) to restart";
fi