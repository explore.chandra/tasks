<?php
$servername = "localhost";
$username = "root";
$password = "12345";

try {
    $conn = new PDO("mysql:host=$servername;dbname=system", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage() . " \n";
}
$numberOfRecords = readline("Plese provide the number(N) to generate data : ");
$insertArray = array();
for ($i = 1; $i <= $numberOfRecords; $i++) {
    $rowData = getUniqueData($conn);
    $value = "('" . $rowData->sapId . "','" . $rowData->hostName . "','" . $rowData->loopBack . "','" . $rowData->macAddress . "','AG1','active')";
    array_push($insertArray, $value);
}
$sql = "INSERT INTO router(sapId,hostName,loopBack,macAddress,type,status) VALUES" . implode(",", $insertArray) . "";
$stmt = $conn->prepare($sql);
$stmt->execute();
echo "Inserted " . $numberOfRecords . " SuccessFully";

function getUniqueData($conn)
{
    $sapId = getRandom(18, 'numeric');
    $hostName = getRandom(14, 'alphabet');
    $loopBack = rand(0, "4294967295");
    $macAddress = implode(':', str_split(substr(md5(mt_rand()), 0, 12), 2));
    $validateQuery = "SELECT * from router WHERE sapId = '" . $sapId . "' OR hostName = '" . $hostName . "' OR loopBack = '" . $loopBack . "' OR macAddress = '" . $macAddress . "' ";
    $result = $conn->query($validateQuery);
    if ($result->rowCount() > 0) {
        getUniqueData($conn);
    } else {
        $response = new \stdClass;
        $response->sapId = $sapId;
        $response->hostName = $hostName;
        $response->loopBack = $loopBack;
        $response->macAddress = $macAddress;
        return $response;
    }
}

function getRandom($n, $type)
{
    if ($type == 'numeric') {
        $characters = '0123456789';
    } else if ($type == 'alphabet') {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    } else {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }
    $randomString = '';
    for ($i = 0; $i < $n; $i++) {
        $index = rand(0, strlen($characters) - 1);
        $randomString .= $characters[$index];
    }
    return $randomString;
}
