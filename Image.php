<?php
$im = imagecreatetruecolor(700, 700);
$white = imagecolorallocate($im, 255, 0, 0);
imageellipse($im, 350, 350, 450, 450, $white);
imagepolygon(
    $im,
    array(
        550, 350,
        350, 550,
        150, 350,
        350, 150,
        550, 350
    ),
    4,
    $white
);
imagepolygon(
    $im,
    array(
        650, 350,
        500, 610,
        200, 610,
        50, 350,
        200, 90,
        500, 90,
        650, 350
    ),
    6,
    $white
);
imagepng($im, 'gemoetric-figure.png');
imagedestroy($im);
