<?php

$totalDiskSpace =  disk_total_space('/');
$totalDiskFree = disk_free_space('/');
$totalDiskUsed = $totalDiskSpace - $totalDiskFree;
echo dataSize($totalDiskUsed);
function dataSize($Bytes)
{
    $Type = array("", "kilo", "mega", "giga", "tera");
    $counter = 0;
    while ($Bytes >= 1024) {
        $Bytes /= 1024;
        $counter++;
    }
    return ("" . $Bytes . " " . $Type[$counter] . "bytes");
}
