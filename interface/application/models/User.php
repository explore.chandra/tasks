<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->userTbl = 'users';
    }

    /*
     * Get rows from the users table
     */
    function getRows($params = array())
    {
        $this->db->select('id,first_name,last_name,email,phone');
        $this->db->from($this->userTbl);

        //fetch data by conditions
        if (array_key_exists("conditions", $params)) {
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        $query = $this->db->get();
        $result = ($query->num_rows() == 1) ? $query->row_array() : $query->result_array();
        return $result;
    }
    public function createKey($userId, $token)
    {
        $data['userId'] = $userId;
        $data['key'] = $token;
        $data['createdAt'] = date("Y/m/d");
        $this->db->insert('keys', $data);
        return $this->db->insert_id();
    }

    public function getKey($token)
    {
        $this->db->from('keys');
        $this->db->where('key', $token);
        $query = $this->db->get();
        return ($query->num_rows());
    }
}
