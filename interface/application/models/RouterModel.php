<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class RouterModel extends CI_Model
{
    public function createRouter($insertObject)
    {
        $data['sapId'] = $insertObject->sapId;
        $data['hostName'] = $insertObject->hostName;
        $data['loopBack'] = $insertObject->loopBack;
        $data['macAddress'] = $insertObject->macAddress;
        $data['type'] = "AG1";
        $data['status'] = "active";
        $this->db->insert('router', $data);
        return $this->db->insert_id();
    }

    public function getRouter($filter = '')
    {
        $this->db->from('router');
        $this->db->where('status', 'active');
        if ($filter != "") {
            if (isset($filter->rowId)) {
                $whereCondition = " id != '" . $filter->rowId . "' AND (sapId = '" . $filter->sapId . "' OR hostName = '" . $filter->hostName . "' OR macAddress = '" . $filter->macAddress . "')";
            }else if(isset($filter->loopBack)){
                $whereCondition = "loopBack != '" . ip2long($filter->loopBack) . "' AND (sapId = '" . $filter->sapId . "' OR hostName = '" . $filter->hostName . "' OR macAddress = '" . $filter->macAddress . "')";
            } else {
                $whereCondition = "sapId = '" . $filter->sapId . "' OR hostName = '" . $filter->hostName . "' OR loopBack = '" . $filter->loopBack . "' OR macAddress = '" . $filter->macAddress . "' ";
            }
            $this->db->where($whereCondition);
        }
        $this->db->order_by('id', "desc");
        $query = $this->db->get();
        return ($query->result());
    }

    function updateRouter($updateObject)
    {
        if (isset($updateObject->sapId))
            $data['sapId'] = $updateObject->sapId;
        if (isset($updateObject->hostName))
            $data['hostName'] = $updateObject->hostName;
        if (isset($updateObject->macAddress))
            $data['macAddress'] = $updateObject->macAddress;
        $data['type'] = "AG1";
        $data['status'] = "active";
        if (isset($updateObject->rowId)) {
            $this->db->where('id', $updateObject->rowId);
        } else if (isset($updateObject->loopBack)) {
            $this->db->where('loopBack', ip2long($updateObject->loopBack));
        }
        $this->db->update('router', $data);
        return $this->db->affected_rows();;
    }

    public function deleteRouter($deleteObject)
    {
        $data['status'] = "disabled";
        if (isset($deleteObject->rowId)) {
            $this->db->where('id', $deleteObject->rowId);
        } else if (isset($deleteObject->loopBack)) {
            $this->db->where('loopBack', ip2long($deleteObject->loopBack));
        }
        $this->db->update('router', $data);
        return $this->db->affected_rows();
    }
}
