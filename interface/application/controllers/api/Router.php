<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Router extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('RouterModel');
        $this->load->model('User');
        $this->unauthorized = false;
    }

    public function router_get($id = "")
    {
        $response = new stdClass;
        $headers = $this->input->request_headers();
        $tokenRes = isset($headers['token']) ? $this->User->getKey($headers['token']) : 0;
        if ($tokenRes > 0) {
            $this->db->select('*,INET_NTOA(loopback) as loopBack');
            if (!empty($id)) {
                $result = $this->db->get_where("router", ['id' => $id])->row_array();
            } else {
                $result = $this->db->get("router")->result();
            }
            if (empty($result)) {
                $response->status = true;
                $response->message = "No Rocords Found !!";
            } else {
                $response->status = true;
                $response->data = $result;
            }
        } else {
            $response->status = false;
            $this->unauthorized  = true;
            $response->message = $this->lang->line('text_rest_unauthorized');
        }
        if ($response->status) {
            $this->response($response, REST_Controller::HTTP_OK);
        } else {
            $this->response($response, ($this->unauthorized) ? REST_Controller::HTTP_UNAUTHORIZED : REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function routerType_get($type = "")
    {
        $response = new stdClass;
        $headers = $this->input->request_headers();
        $tokenRes = isset($headers['token']) ? $this->User->getKey($headers['token']) : 0;
        if ($tokenRes > 0) {
            if (!empty($type)) {
                $this->db->select('*,INET_NTOA(loopback) as loopBack');
                $result = $this->db->get_where("router", ['type' => $type])->result();
            } else {
                $response->status = false;
                $response->message = "Please select type";
            }
            if (empty($result)) {
                $response->status = false;
                $response->message = "No Rocords Founds";
            } else {
                $response->status = true;
                $response->data = $result;
            }
        } else {
            $response->status = false;
            $this->unauthorized = true;
            $response->message = $this->lang->line('text_rest_unauthorized');
        }
        if ($response->status) {
            $this->response($response, REST_Controller::HTTP_OK);
        } else {
            $this->response($response, ($this->unauthorized) ? REST_Controller::HTTP_UNAUTHORIZED : REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function routerBetween_get($from, $to)
    {
        $response = new stdClass;
        $headers = $this->input->request_headers();
        $tokenRes = isset($headers['token']) ? $this->User->getKey($headers['token']) : 0;
        if ($tokenRes > 0) {
            $this->db->select('*,INET_NTOA(loopback) as loopBack');
            if (!empty($from) && !empty($to)) {
                $this->db->order_by('loopBack', 'ASC');
                $result = $this->db->get_where("router", [
                    'loopBack >= ' => ip2long($from),
                    'loopBack <= ' => ip2long($to)
                ])->result();
            } else {
                $response->status = false;
                $response->message = "Please Provide Range";
            }
            if (empty($result)) {
                $response->status = false;
                $response->message = "No Rocords Founds";
            } else {
                $response->status = true;
                $response->data = $result;
            }
        } else {
            $response->status = false;
            $this->unauthorized = true;
            $response->message = $this->lang->line('text_rest_unauthorized');
        }
        if ($response->status) {
            $this->response($response, REST_Controller::HTTP_OK);
        } else {
            $this->response($response, ($this->unauthorized) ? REST_Controller::HTTP_UNAUTHORIZED : REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function router_post()
    {
        $result = new \stdClass;
        $headers = $this->input->request_headers();
        $tokenRes = isset($headers['token']) ? $this->User->getKey($headers['token']) : 0;
        if ($tokenRes > 0) {
            $sapId = $this->input->post('sapId');
            $hostName = $this->input->post('hostName');
            $loopBack = $this->input->post('loopBack');
            $macAddress = $this->input->post('macAddress');
            if (!empty($sapId) && !empty($hostName) && !empty($loopBack) && !empty($macAddress)) {
                $inserObject = new \stdClass;
                $inserObject->sapId = $sapId;
                $inserObject->hostName = $hostName;
                $inserObject->loopBack = $loopBack;
                $inserObject->macAddress = $macAddress;
                $alreadyExist = $this->RouterModel->getRouter($inserObject);
                if ($alreadyExist) {
                    $result->status = false;
                    $result->message = "Router Details Already Exist";
                } else {
                    $response = $this->RouterModel->createRouter($inserObject);
                    if ($response) {
                        $result->status = true;
                        $result->message = "Inserted Successfully";
                    } else {
                        $result->status = true;
                        $result->message = "Failed Inserting";
                    }
                }
            } else {
                $result->status = false;
                $result->message = "Router Details are Required";
            }
        } else {
            $result->status = false;
            $this->unauthorized = true;
            $result->message = $this->lang->line('text_rest_unauthorized');
        }
        if ($result->status) {
            $this->response($result, REST_Controller::HTTP_OK);
        } else {
            $this->response($result, ($this->unauthorized) ? REST_Controller::HTTP_UNAUTHORIZED : REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    public function router_put()
    {
        $result = new \stdClass;
        $headers = $this->input->request_headers();
        $tokenRes = isset($headers['token']) ? $this->User->getKey($headers['token']) : 0;
        if ($tokenRes > 0) {
            $rowId = $this->put('rowId');
            $sapId = $this->put('sapId');
            $hostName = $this->put('hostName');
            $loopBack = $this->put('loopBack');
            $macAddress = $this->put('macAddress');
            $updateObject = new \stdClass;
            if (isset($rowId))
                $updateObject->rowId = $rowId;
            $updateObject->sapId = $sapId;
            $updateObject->hostName = $hostName;
            $updateObject->loopBack = $loopBack;
            $updateObject->macAddress = $macAddress;
            $alreadyExist = $this->RouterModel->getRouter($updateObject);

            if (!empty($rowId) || !empty($loopBack)) {
                if ($alreadyExist) {
                    $result->status = false;
                    $result->message = "Router Details Already Exist";
                } else {
                    $response = $this->RouterModel->updateRouter($updateObject);
                    if ($response) {
                        $result->status = true;
                        $result->message = "Updated Successfully";
                    } else {
                        $result->status = true;
                        $result->message = "No Changes to update";
                    }
                }
            } else {
                $result->status = false;
                $result->message = "Please Provide id / loopBack";
            }
        } else {
            $result->status = false;
            $this->unauthorized = true;
            $result->message = $this->lang->line('text_rest_unauthorized');
        }
        if ($result->status) {
            $this->response($result, REST_Controller::HTTP_OK);
        } else {
            $this->response($result, ($this->unauthorized) ? REST_Controller::HTTP_UNAUTHORIZED : REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function router_delete($loopBack)
    {
        $updateObject = new \stdClass;
        $result = new \stdClass;
        $headers = $this->input->request_headers();
        $tokenRes = isset($headers['token']) ? $this->User->getKey($headers['token']) : 0;
        if ($tokenRes > 0) {
            $updateObject->loopBack = $loopBack;
            $response = $this->RouterModel->deleteRouter($updateObject);
            if ($response) {
                $result->status = true;
                $result->message = "Deleted Successfully";
            } else {
                $result->status = true;
                $result->message = "Delete Failed";
            }
        } else {
            $result->status = false;
            $this->unauthorized = true;
            $result->message = $this->lang->line('text_rest_unauthorized');
        }
        if ($result->status) {
            $this->response($result, REST_Controller::HTTP_OK);
        } else {
            $this->response($result, ($this->unauthorized) ? REST_Controller::HTTP_UNAUTHORIZED : REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}
