<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Authentication extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User');
    }

    public function login_post()
    {
        $response = new stdClass;
        $response->status = true;
        $email = $this->post('email');
        $password = $this->post('password');
        if (!empty($email) && !empty($password)) {
            $con['conditions'] = array(
                'email' => $email,
                'password' => md5($password),
                'status' => 1
            );
            $user = $this->User->getRows($con);
            if ($user) {
                $token = $this->getRandom();
                $response->message = "Login Success";
                $response->data = $user;
                $response->token = $token;
                $this->User->createKey($user['id'], $token);
            } else {
                $response->status = false;
                $response->message = "Wrong email or password.";
            }
        } else {
            $response->status = false;
            $response->message = "Provide email and password.";
        }
        if ($response->status) {
            $this->response($response, REST_Controller::HTTP_OK);
        } else {
            $this->response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
    }


    function getRandom()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $n = 100;
        $randomString = '';
        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }
        return $randomString;
    }
}
