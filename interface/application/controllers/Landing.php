<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Landing extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('RouterModel');
    }
    public function index()
    {
        $data['pageName'] = 'Create';
        $data['routerDetails'] = $this->RouterModel->getRouter();
        $this->load->view('landing', $data);
    }
    public function create()
    {
        $result = new \stdClass;
        $sapId = $this->input->post('sapId');
        $hostName = $this->input->post('hostName');
        $loopBack = $this->input->post('loopBack');
        $macAddress = $this->input->post('macAddress');
        if (!empty($sapId) && !empty($hostName) && !empty($loopBack) && !empty($macAddress)) {
            $inserObject = new \stdClass;
            $inserObject->sapId = $sapId;
            $inserObject->hostName = $hostName;
            $inserObject->loopBack = $loopBack;
            $inserObject->macAddress = $macAddress;
            $alreadyExist = $this->RouterModel->getRouter($inserObject);

            if ($alreadyExist) {
                $result->status = false;
                $result->message = "Router Details Already Exist";
            } else {
                $response = $this->RouterModel->createRouter($inserObject);
                if ($response) {
                    $result->status = true;
                    $result->message = "Inserted Successfully";
                } else {
                    $result->status = true;
                    $result->message = "Failed Inserting";
                }
            }
        } else {
            $result->status = false;
            $result->message = "Router Details are Required";
        }
        echo json_encode($result);
    }

    public function get()
    {
        $result = $this->RouterModel->getRouter();
        return json_encode($result);
    }
    function update()
    {
        $rowId = $this->input->post('rowId');
        $sapId = $this->input->post('sapId');
        $hostName = $this->input->post('hostName');
        $loopBack = $this->input->post('loopBack');
        $macAddress = $this->input->post('macAddress');
        $updateObject = new \stdClass;
        $updateObject->rowId = $rowId;
        $updateObject->sapId = $sapId;
        $updateObject->hostName = $hostName;
        $updateObject->loopBack = $loopBack;
        $updateObject->macAddress = $macAddress;
        $alreadyExist = $this->RouterModel->getRouter($updateObject);
        $result = new \stdClass;
        if ($alreadyExist) {
            $result->status = false;
            $result->message = "Router Details Already Exist";
        } else {
            $response = $this->RouterModel->updateRouter($updateObject);
            if ($response) {
                $result->status = true;
                $result->message = "Updated Successfully";
            } else {
                $result->status = true;
                $result->message = "No Chnages to Update";
            }
        }
        echo json_encode($result);
    }

    function delete()
    {
        $rowId = $this->input->post('rowId');
        $updateObject = new \stdClass;
        $updateObject->rowId = $rowId;
        $result = new \stdClass;
        $response = $this->RouterModel->deleteRouter($updateObject);
        if ($response) {
            $result->status = true;
            $result->message = "Deleted Successfully";
        } else {
            $result->status = true;
            $result->message = "This Record is Already Deleted";
        }
        echo json_encode($result);
    }
}
