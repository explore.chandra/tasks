<!doctype html>
<html lang="en">

<head>
    <title>Interface</title>
    <link href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>/assets/css/jquery.dataTables.min.css" rel="stylesheet">
    <style>
        .error {
            font-size: 12px;
            color: red;
        }
    </style>
</head>

<body>

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Router Information <span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="row m-md-5">
        <h1>
            <span id="actionName">
                <?php echo $pageName; ?>
            </span>
        </h1>
        <div class="col-md-12">
            <form id="form-create" name="form-create">
                <div class="row m-2">
                    <div class="col-md-4">
                        <label class="label"> Sap ID </label> : <input class="form-control-sm" type="text" id="sapid" name="sapId" placeholder="Enter SAP ID" />
                    </div>
                    <div class="col-md-4">
                        <label class="label"> Hostname </label> : <input class="form-control-sm" type="text" id="hostname" name="hostName" placeholder="Enter Hostname" />
                    </div>
                    <div class="col-md-4">
                        <label class="label"> Loopback </label> : <input class="form-control-sm" type="text" id="loopback" name="loopBack" placeholder="Enter Loopback" />
                    </div>
                    <div class="col-md-4">
                        <label class="label"> MAC Address </label> : <input class="form-control-sm" type="text" id="macaddress" name="macAddress" placeholder="Enter MAC Address" />
                    </div>
                    <div class="col-md-8 text-right">
                        <button class="btn btn-success" id="create">Create Router</button>
                        <button class="btn  btn-primary" id="update" style="display: none;">Update Router</button>
                        <button class="btn  btn-bd-primary" id="switch" style="display: none;" title="create new router">Switch To Create Router</button>
                    </div>
                    <input type="hidden" id="updateId">
                </div>
            </form>
        </div>
        <div class="col-md-12 mt-3">
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>SAP ID</th>
                        <th>Hostname</th>
                        <th>Loopback</th>
                        <th>MAC Address</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($routerDetails as $router) {
                    ?>
                        <tr>
                            <td class="text-center"><?php echo $router->sapId; ?> <input type="hidden" id="<?php echo $router->id; ?>_sap_id" value="<?php echo $router->sapId; ?>"></td>
                            <td class="text-center"><?php echo $router->hostName; ?> <input type="hidden" id="<?php echo $router->id; ?>_host_name" value="<?php echo $router->hostName; ?>"></td>
                            <td class="text-center"><?php echo long2ip($router->loopBack); ?> <input type="hidden" id="<?php echo $router->id; ?>_loop_back" value="<?php echo long2ip($router->loopBack); ?>"></td>
                            <td class="text-center"><?php echo $router->macAddress; ?> <input type="hidden" id="<?php echo $router->id; ?>_mac_address" value="<?php echo $router->macAddress; ?>"></td>
                            <td class="text-center">
                                <button class="btn edit-button" data-id="<?php echo $router->id; ?>" onclick="updateRouter(this)"> <i class="fa fa-pencil fa-fw" aria-hidden="true"></i></button>
                                <button class="btn delete-button" data-id="<?php echo $router->id; ?>" onclick="deleteRouter(this)"> <i class="fa fa-trash" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <script src="<?php echo base_url(); ?>/assets/js/jquery-3.5.1.min.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/jquery.dataTables.min.js"></script>
</body>
<script>
    let base_url = "<?php echo base_url(); ?>";
    $(document).ready(function() {
        $('#example thead tr').clone(true).appendTo('#example thead');
        $('#example thead tr:eq(1) th').each(function(i) {
            var title = $(this).text();
            if (title != "Action") {
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');
                $('input', this).on('keyup change', function() {
                    console.log(this.value);
                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            } else {
                $(this).html('<span>Search Not Required </span>');
            }
        });
        var table = $('#example').DataTable({
            ordering: false,
            searching: true
        });
    });
    let validateForm = function() {
        let validator = $("#form-create").validate({
            rules: {
                sapId: "required",
                hostName: "required",
                loopBack: "required",
                macAddress: "required"
            },
            messages: {
                sapId: "Please Enter SAP ID",
                hostName: "Please Enter Hostname",
                loopBack: "Please Enter Loopback",
                macAddress: "Please Enter MAC Address"
            },
            submitHandler: function() {}
        });
        if (validator.form()) {
            return true;
        } else {
            return false;
        }
    }
    $('#create').click(function() {
        let validationResponse = validateForm();
        if (validationResponse) {
            $.ajax({
                type: "POST",
                url: base_url + "create",
                dataType: "json",
                data: {
                    sapId: $('#sapid').val(),
                    hostName: $('#hostname').val(),
                    loopBack: $('#loopback').val(),
                    macAddress: $('#macaddress').val()
                },
                success: function(data) {
                    let response = data;
                    console.log(response);
                    if (response.status) {
                        alert(response.message);
                        setTimeout(window.location.reload(), 3000);
                    } else {
                        alert(response.message);
                    }
                },
                error: function(errorThrown) {
                    console.log(errorThrown);
                }
            });
        }
    });

    function updateRouter(element) {
        let rowId = $(element).data("id");
        $('#updateId').val(rowId);
        $('#sapid').val($('#' + rowId + '_sap_id').val());
        $('#hostname').val($('#' + rowId + '_host_name').val());
        $('#loopback').val($('#' + rowId + '_loop_back').val());
        $('#macaddress').val($('#' + rowId + '_mac_address').val());
        $('#create').hide();
        $('#update').show();
        $('#switch').show();
    }

    function deleteRouter(element) {
        let rowId = $(element).data("id");
        console.log(rowId);
    }
    $('#switch').click(() => {
        $('#create').show();
        $('#update').hide();
        $('#switch').hide();
        $('#sapid').val('');
        $('#hostname').val('');
        $('#loopback').val('');
        $('#macaddress').val('');
    })
    $('#update').click(function() {
        let validationResponse = validateForm();
        if (validationResponse) {
            $.ajax({
                type: "POST",
                url: base_url + "update",
                dataType: "json",
                data: {
                    rowId: $('#updateId').val(),
                    sapId: $('#sapid').val(),
                    hostName: $('#hostname').val(),
                    loopBack: $('#loopback').val(),
                    macAddress: $('#macaddress').val()
                },
                success: function(data) {
                    let response = data;
                    console.log(response);
                    if (response.status) {
                        alert(response.message);
                        setTimeout(window.location.reload(), 3000);
                    } else {
                        alert(response.message);
                    }
                },
                error: function(errorThrown) {
                    console.log(errorThrown);
                }
            });
        }
    });

    function deleteRouter(element) {
        let rowId = $(element).data("id");
        $.ajax({
            type: "POST",
            url: base_url + "delete",
            dataType: "json",
            data: {
                rowId: rowId
            },
            success: function(data) {
                let response = data;
                console.log(response);
                if (response.status) {
                    alert(response.message);
                    setTimeout(window.location.reload(), 3000);
                } else {
                    alert(response.message);
                }
            },
            error: function(errorThrown) {
                console.log(errorThrown);
            }
        });
    }
</script>

</html>